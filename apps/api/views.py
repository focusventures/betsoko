from django.db.models import Sum
from apps.games.models import Fixtures, League, Country, Season, Team, Events, Player, LeagueTable
from apps.activity.models import Betslip, Prediction
from django.contrib.auth.models import User
# from apps.authorise.models import CustomUser
from apps.wallet.models import Debits, Credits
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status


from rest_framework import viewsets
from apps.api.serializers.games import FixturesSerializer, LeaguesSerializer, CountriesSerializer, TeamsSerializer
from apps.api.serializers.activity import BetslipSerializer, PredictionsSerializer
from apps.api.serializers.authorise import ProfileSerializer
from apps.api.serializers.wallet import DebitsSerializer, CreditsSerializer


class TeamsViewSet(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamsSerializer

class CountriesViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountriesSerializer


class LeaguesViewSet(viewsets.ModelViewSet):
    queryset = League.objects.all()
    serializer_class = LeaguesSerializer


class FixturesViewSet(viewsets.ModelViewSet):
    queryset = Fixtures.objects.all()
    serializer_class = FixturesSerializer


class BetslipViewSet(viewsets.ModelViewSet):
    queryset = Betslip.objects.all()
    serializer_class = BetslipSerializer


class PredictionsViewSet(viewsets.ModelViewSet):
    queryset = Prediction.objects.all()
    serializer_class = PredictionsSerializer


class ProfilesViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = ProfileSerializer

class DebitsViewSet(viewsets.ModelViewSet):
    queryset = Debits.objects.all()
    serializer_class = DebitsSerializer



class CreditsViewSet(viewsets.ModelViewSet):
    queryset = Credits.objects.all()
    serializer_class = CreditsSerializer


@api_view(['POST'])
def upKicks(request):
    if request.method == 'POST':
        
        print(request.POST)
        # betslip = Betslip.objects.get(id=id)
        # kicks = betslip.up_kicks
        # kicks += 1
        # betslip.up_kicks == kicks
        # betslip.save()
        print("Success")
        return Response("success")


@api_view(['POST'])
def downKicks(request):
    betslip = Betslip.objects.get(id=id)
    kicks = betslip.down_kicks
    kicks += 1
    betslip.down_kicks == kicks
    betslip.save()
    print("Success")
    return "success"


@api_view(['POST'])
def buy(request):
    if request.method == 'POST':
        print(request.POST)
        # allDebits = Debits.objects.filter(user=request.user)
        # allCredits = Credits.objects.filter(user=request.user)
        DebitsTotals = Debits.objects.aggregate(Sum('amount'))
        CreditsTotals = Credits.objects.aggregate(Sum('amount'))
        balance  = CreditsTotals['amount__sum']  - DebitsTotals['amount__sum']
        print(DebitsTotals, CreditsTotals)
        print(balance)
        # betslip = Betslip.objects.get(id=request.POST['id'])

    
    return Response("success")


@api_view(['GET'])
def mybetslips(request):
    if request.method == 'GET':
        betslips = Betslip.objects.filter(user=request.user)

        print(betslips)
        

    
    return Response("success")

