from django.urls import path, include

from rest_framework import routers

from apps.api import views


routers = routers.DefaultRouter()
routers.register(r'teams', views.TeamsViewSet)
routers.register(r'fixtures', views.FixturesViewSet)
routers.register(r'leagues', views.LeaguesViewSet)
routers.register(r'countries', views.CountriesViewSet)
routers.register(r'betslips', views.BetslipViewSet)
# routers.register(r'betslips/upkicks', views.upKicks, base_name='upkicks')
# routers.register(r'betslips/downkicks', views.downKicks, base_name='downkicks')
routers.register(r'predictions', views.PredictionsViewSet)
routers.register(r'profiles', views.ProfilesViewSet)
routers.register(r'wallet/debits', views.DebitsViewSet)
routers.register(r'wallet/credits', views.CreditsViewSet)
# routers.register(r'wallet/buy', views.ProfilesViewSet)




urlpatterns = routers.urls

more = [
    path(r'wallet/buy', views.buy),
    path(r'betslips/upkicks', views.upKicks, name='upkicks'),
    path(r'betslips/downkicks', views.downKicks, name='downkicks'),
    path(r'betslips/mybetslips', views.mybetslips, name='mybetslips')
]

urlpatterns += more