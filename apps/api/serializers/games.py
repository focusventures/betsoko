from django.shortcuts import render
from rest_framework import serializers

from apps.games.models import Fixtures, League, Country, Team

class LeaguesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = League
        fields = ("league_id", "name", "country_code", "country", "season", "season_start","season_end", "flag", "standings", "logo")


class TeamsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Team
        fields = ("league", "team_id", "name", "code", "logo")

class FixturesSerializer(serializers.HyperlinkedModelSerializer):
    homeTeam = TeamsSerializer()
    awayTeam = TeamsSerializer()
    league_id = LeaguesSerializer()
    
    class Meta:
        model = Fixtures
        fields = ("fixture_id", "league_id", "event_date", "homeTeam", "awayTeam", "fixture_round", "event_timestamp", "event_date", "statusShort", "status", "final_score", "penalty")




class CountriesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields = ("countyry_id", "name")







