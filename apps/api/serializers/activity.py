from django.shortcuts import render
from rest_framework import serializers

from apps.activity.models import Betslip, Prediction
from apps.api.serializers.games import FixturesSerializer
from apps.api.serializers.authorise import UserSerializer



class PredictionsSerializer(serializers.ModelSerializer):
    fixture = FixturesSerializer()

    class Meta:
        model = Prediction
        fields = ( "fixture",  "created_on","status", "result")


class BetslipSerializer(serializers.ModelSerializer):
    predictions = PredictionsSerializer(many=True)
    user = UserSerializer()
    class Meta:
        model = Betslip
        fields = ( "id", "user", "name", "date_created", "predictions", "up_kicks", "down_kicks", "sales", "free", "active")



