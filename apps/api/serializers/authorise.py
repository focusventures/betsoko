from django.shortcuts import render
from rest_framework import serializers
from django.contrib.auth.models import User
# from apps.authorise.models import CustomUser





class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', )

class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ("username", )





