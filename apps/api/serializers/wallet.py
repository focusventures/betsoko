from django.shortcuts import render
from rest_framework import serializers

from apps.wallet.models import Debits, Credits



class DebitsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Debits
        fields = ()


class CreditsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Credits
        fields = ()

