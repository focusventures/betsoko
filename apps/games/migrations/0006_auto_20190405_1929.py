# Generated by Django 2.1.7 on 2019-04-05 16:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0005_auto_20190405_1911'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fixtures',
            name='event_date',
            field=models.DateField(null=True, verbose_name='Event Date'),
        ),
        migrations.AlterField(
            model_name='fixtures',
            name='event_timestamp',
            field=models.DateField(null=True, verbose_name='Event TimeStamp'),
        ),
    ]
