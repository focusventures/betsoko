from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.


class Country(models.Model):
    countyry_id = models.IntegerField(_("Country"))
    name = models.CharField(_("Country Name"), max_length=50)

class League(models.Model):
    league_id = models.IntegerField()
    name = models.CharField(max_length=30, blank=True, null=True)
    country_code = models.CharField(max_length=20, blank=True, null=True)
    country = models.ForeignKey('games.Country', related_name='Country', on_delete=models.CASCADE)
    season = models.CharField(_("Season"), max_length=50)
    season_start = models.DateField(_("Start Date"), auto_now=False, auto_now_add=False)
    season_end = models.DateField(_("End Date"), auto_now=False, auto_now_add=False)
    flag = models.CharField(_("Flag"), max_length=50)
    standings = models.BooleanField(_("Standings"))
    logo = models.CharField(_("Logo URi"), max_length=50)

    def __unicode__(self):
        return "{}".format(self.name)

class Team(models.Model):
    league = models.ForeignKey("games.League", verbose_name=_("League Name"), on_delete=models.CASCADE, default=None)
    team_id = models.IntegerField(_("Team Id"))
    name = models.CharField(_("Team name"), max_length=50)
    code = models.CharField(_("Code"), max_length=10, blank=True, null=True)
    logo = models.CharField(_("Logo URi"), max_length=50)

    def __str__(self):
        return "{}".format( self.name) 

class Season(models.Model):
    season_id = models.IntegerField(_("Season Id"))
    year = models.IntegerField(_("Year"))

class Fixtures(models.Model):
    fixture_id = models.IntegerField(_("Fixture Id"))
    event_timestamp = models.CharField(_("Event TimeStamp"), max_length=50,null=True)
    event_date = models.CharField(_("Event Date"), max_length=50, null=True)
    league_id = models.ForeignKey("games.League", verbose_name=_("League"), on_delete=models.CASCADE)
    fixture_round = models.CharField(_("Round"), max_length=50)
    homeTeam = models.ForeignKey("games.Team", verbose_name=_("Team"), related_name="HomeTeam", on_delete=models.CASCADE)
    awayTeam = models.ForeignKey("games.Team", verbose_name=_("Team"), related_name="AwayTeam", on_delete=models.CASCADE)
    status = models.CharField(_("Status"), max_length=50)
    statusShort = models.CharField(_("Status Short"), max_length=10)
    goalsHomeTeam = models.IntegerField(_("GoalsHomeTeam"), default=0, null=True)
    goalsAwayTeam = models.IntegerField(_("GoalsAwayTeam"), default=0, null=True)
    halftime_score = models.CharField(_("Half Time Score"), max_length=50, null=True)
    final_score = models.CharField(_("Final Score"), max_length=50, null=True)
    penalty = models.IntegerField(_("Penalty"), default=0, null=True)
    elapsed = models.IntegerField(_("Elapsed"), default=0 , null=True)
    firstHalfstart = models.CharField(_("First Half Start"), max_length=50, default=None)
    secondHalfStart = models.CharField(_("Second Half Start"), max_length=50, default=None)


    def __str__(self):
        return "{}  vs {}".format(self.awayTeam, self.homeTeam) 

class Events(models.Model):
    elasped = models.IntegerField(_("Elapsed"))
    teamName = models.CharField(_("Team Name"), max_length=50)
    player = models.ForeignKey("games.Player", verbose_name=_("Player"), on_delete=models.CASCADE)
    event_type = models.CharField(_("Type"), max_length=50)
    detail = models.CharField(_("Type Detail"), max_length=50)

class Player(models.Model):
    name = models.CharField(_("Player Name"), max_length=50)
    number = models.IntegerField(_("Player Number"))

class LeagueTable(models.Model):
    rank = models.IntegerField(_("Rank"))
    teamName = models.CharField(_("Team Name"), max_length=50)
    matchesPlayed = models.IntegerField(_("Matches Played"))
    win = models.IntegerField(_("Win"))
    draw = models.IntegerField(_("Draw"))
    lose = models.IntegerField(_("Lose"))
    goalsFor = models.IntegerField(_("GoalsFor"))
    goalsAgainst = models.IntegerField(_("Goals Against"))
    goalsDiff = models.IntegerField(_("Goals Diff"))
    points = models.IntegerField(_("Points"))
    group = models.CharField(_("Group"), max_length=50)
    lastUpdate = models.DateField(_(""), auto_now=True)


