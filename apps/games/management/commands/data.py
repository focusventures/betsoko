from django.core.management.base import BaseCommand
# from apps.games.models import Fixtures, Seasons, 
#         Teams, Players, Events, 
#         League, Country,LeagueTable 
from apps.games.models import Fixtures, League, Country, Team

import requests

base_url = "https://api-football-v1.p.rapidapi.com/"



class Command(BaseCommand):
    help = "Get footbal data from https://www.api-football.com/demo"


    def add_arguments(self, parser):
        parser.add_argument(
            '--season', dest='season', required=True,
            help='season is required eg --season 2018',
        )

    def handle(self, *args, **options):
        season = options['season']
       
        # self.get_countries(season)
        # self.get_leagues(season)
        # self.get_teams(season)
        self.get_fixtures(season)
        
        req = requests.get(base_url + '/fixtures', headers={"X-RapidAPI-Key": "90914a4d2bmsh9d1f5f75faf2289p1277d2jsn89045c2205c8"})

        
    def get_leagues(self, season):
        uri = base_url + "leagues/season/" + season
        req = requests.get(uri, headers={"X-RapidAPI-Key": "90914a4d2bmsh9d1f5f75faf2289p1277d2jsn89045c2205c8"})
        
        for key, value in req.json()["api"]["leagues"].items():
            country = Country.objects.get(name=value['country'])
            League(league_id=value['league_id'], name=value['name'],
            country=country, country_code=value['country_code'],
            season=value['season'], season_start=value['season_start'],
            season_end=value['season_end'], flag=value['flag'],
            logo=value['logo'], standings=value['standings']
            ).save()
            print(value)

    def get_countries(self, season):
        uri = base_url + "countries/" + season
        req = requests.get(uri, headers={"X-RapidAPI-Key": "90914a4d2bmsh9d1f5f75faf2289p1277d2jsn89045c2205c8"})
        for key, value in req.json()["api"]["countries"].items():
            Country(countyry_id=key, name=value,           
            ).save()


    def get_teams(self, season):
        leagues = League.objects.all()
        for league in leagues:
            uri = base_url + "teams/league/" + str(league.league_id)
            req = requests.get(uri, headers={"X-RapidAPI-Key": "90914a4d2bmsh9d1f5f75faf2289p1277d2jsn89045c2205c8"})
            print("got response")
            for key, value in req.json()["api"]["teams"].items():
                Team(league=league, team_id=value["team_id"],
                name=value["name"],logo=value["logo"], code=value["code"]
                ).save()



    def get_fixtures(self, season):
        leagues = League.objects.all()
        for league in leagues:
            print("getting fixtures")
            uri = base_url + "fixtures/league/" + str(league.league_id)
            req = requests.get(uri, headers={"X-RapidAPI-Key": "90914a4d2bmsh9d1f5f75faf2289p1277d2jsn89045c2205c8"})
           
            for key, value in req.json()["api"]["fixtures"].items():
                print(value)
                league = League.objects.get(league_id=league.league_id)
                home = Team.objects.get(name=value["homeTeam"])
                away = Team.objects.get(name=value["awayTeam"])
                               
                Fixtures(fixture_id=value["fixture_id"], 
                event_timestamp=value["event_timestamp"],
                event_date=value["event_date"],
                league_id=league, fixture_round=value["round"],
                homeTeam=home, awayTeam=away,
                status=value["status"], statusShort=value["statusShort"],
                goalsHomeTeam=value["goalsHomeTeam"], goalsAwayTeam=value["goalsAwayTeam"],
                halftime_score=value["halftime_score"], final_score=value["final_score"],
                penalty=value["penalty"], elapsed=value["elapsed"],
                firstHalfstart=value["firstHalfStart"],
                secondHalfStart=value['secondHalfStart']
                 ).save()
                print("Fixture Added")


            
       
            
        




        






