from django.contrib import admin
from .models import League, Country, Fixtures, Events, Team, Season, Player, LeagueTable

class FixtureModelAdmin(admin.ModelAdmin):
    list_display = ('fixture_id', 'homeTeam', 'awayTeam', 'league_id')

admin.site.register(League)
admin.site.register(Country)
admin.site.register(Fixtures, FixtureModelAdmin)
admin.site.register(Events)
admin.site.register(Player)
admin.site.register(Season)
admin.site.register(LeagueTable)
admin.site.register(Team)
# Register your models here.
