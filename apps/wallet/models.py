from django.db import models
from django.utils.translation import ugettext_lazy as _


class Credits(models.Model):
    user = models.ForeignKey("auth.User", verbose_name=_("Crediting"), on_delete=models.CASCADE)
    date_deposited = models.DateTimeField(_("Date Of The Deposit"), auto_now=True)
    amount = models.FloatField(_("Amount Deposited"), default=0.0)
    mode = models.CharField(_("Mode Used"), max_length=50)



class Debits(models.Model):
    user = models.ForeignKey("auth.User", verbose_name=_("Debiting"), on_delete=models.CASCADE)
    date_deposited = models.DateTimeField(_("Date Of The Debiting"), auto_now=True)
    amount = models.FloatField(_("Amount Debitted"), default=0.0)
    mode = models.CharField(_("Mode Used"), max_length=50)



