from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Betslip(models.Model):
    user = models.ForeignKey("auth.User", verbose_name=_("Created By"), on_delete=models.CASCADE)
    name = models.CharField(_("Betslip Title"), max_length=50)
    date_created = models.DateTimeField(_("Date Created"), auto_now=True)
    predictions = models.ManyToManyField("activity.Prediction", verbose_name=_("Prediction"))
    up_kicks = models.IntegerField(_("Up Kicks"), default=0)
    down_kicks = models.IntegerField(_("Down Kicks"), default=0)
    sales = models.IntegerField(_("Sales Made"))
    free = models.BooleanField(_("Offered for free"), default=False)
    active = models.BooleanField(_("Bet is active or not"), default=True)


    def __str__(self):
        return "{} predicted {}".format(self.user, self.name) 




class Prediction(models.Model):
    fixture = models.ForeignKey("games.Fixtures", verbose_name=_("Fixture"), on_delete=models.CASCADE)
    created_by = models.ForeignKey("auth.User", verbose_name=_("Created By"), on_delete=models.CASCADE)
    created_on = models.DateTimeField(_("Created On"), auto_now=True)
    status = models.CharField(_("Status"), max_length=50)
    result = models.CharField(_("Result"), max_length=50)

