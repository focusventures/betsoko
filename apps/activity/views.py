from django.shortcuts import render
from .models import Betslip, Prediction


def index(request):
    context = {}
    betslips = Betslip.objects.all()
    context['betslips']= betslips
    return render(request, 'activity/index.html', context)


def create_betslip(request):
    return render(request, 'activity/create_betslip.html')


def create_jackpotslip(request):
    return render(request, 'activity/create_jackpotslip.html')


def buy_betslip(request):
    return render(request, 'activity/buy.html')



