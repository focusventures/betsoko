from django.contrib import admin

from .models import Betslip, Prediction



admin.site.register(Betslip)
admin.site.register(Prediction)
