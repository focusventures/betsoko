# Generated by Django 2.1.7 on 2019-04-01 13:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('games', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Betslip',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, verbose_name='Betslip Title')),
                ('date_created', models.DateTimeField(auto_now=True, verbose_name='Date Created')),
                ('up_kicks', models.IntegerField(default=0, verbose_name='Up Kicks')),
                ('down_kicks', models.IntegerField(default=0, verbose_name='Down Kicks')),
                ('sales', models.IntegerField(verbose_name='Sales Made')),
                ('free', models.BooleanField(default=False, verbose_name='Offered for free')),
                ('active', models.BooleanField(default=True, verbose_name='Bet is active or not')),
            ],
        ),
        migrations.CreateModel(
            name='Prediction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now=True, verbose_name='Created On')),
                ('status', models.CharField(max_length=50, verbose_name='Status')),
                ('result', models.CharField(max_length=50, verbose_name='Result')),
                ('created_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Created By')),
                ('fixture', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='games.Fixtures', verbose_name='Fixture')),
            ],
        ),
        migrations.AddField(
            model_name='betslip',
            name='predictions',
            field=models.ManyToManyField(to='activity.Prediction', verbose_name='Prediction'),
        ),
        migrations.AddField(
            model_name='betslip',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Created By'),
        ),
    ]
