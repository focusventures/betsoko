from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator

# from phonenumber_field.modelfields import PhoneNumberField
from apps.wallet.models import Debits, Credits
from apps.activity.models import Betslip


class CustomUserManager(UserManager):
    pass


class CustomUser(AbstractUser):
    firstname = models.CharField(_("First Name"), max_length=50)
    lastname = models.CharField(_("Last Name"), max_length=50)
    phoneregex = RegexValidator(regex=r'^07\d{8}')
    phonenumber = models.CharField(validators=[phoneregex], max_length=10, unique=True, null=True)
    dateofbirth = models.DateField(_("Date Of Birth"), auto_now=False, blank=True, null=True)

    USERNAME_FIELD = 'phonenumber'
    objects = CustomUserManager()

    def __str__(self):
        return "{} {}".format(self.username, self.phonenumber)


# class Profile(CustomUser):
#     # user = models.ForeignKey("authorise.User", verbose_name=_("User"), on_delete=models.CASCADE)
#     # profile_pic = models.ImageField(_("Profile Image"), upload_to=None, height_field=None, width_field=None, max_length=None)
#     reputation = models.IntegerField(_("Reputation"))


#     def get_full_name(self):
#         return "{} {}".format(self.user.firstname, self.user.lastname)

#     def get_phone_number(self):
#         return self.user.phonenumber

#     def get_betslips(self):
#         user_betslips = Betslip.objects.all()
#         return user_betslips

#     def active_betslips(self):
#         active_slips = Betslip.objects.all()

#     def get_debits(self):
#         user_deposits = Debits.objects.all()
#         return user_deposits

#     def get_credits(self):
#         user_credits = Credits.objects.all()
#         return user_credits

#     def get_account_balance(self):
#         user_account_balance = sum(self.get_credits) - sum(self.get_debits)
#         return user_account_balance

#     def reputation(self):
#         betslips = self.get_betslips

#         for slip in betslips:
#             all_true = 0
#             all_false = 0
#             for prediction in slip.predictions:
#                 pass

                
