from django import forms 
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from apps.authorise.models import CustomUser

class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username',  'firstname', 'lastname', 'dateofbirth')


class CustomUserChangeForm(UserChangeForm):
    
    class Meta:
        model = CustomUser
        fields = ('username',  'firstname', 'lastname', 'dateofbirth')

