import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../services/wallet.service';

@Component({
  selector: 'app-debits',
  templateUrl: './debits.component.html',
  styleUrls: ['./debits.component.scss']
})
export class DebitsComponent implements OnInit {
  allDebits = [];
  constructor(private walletApi: WalletService) {
    this.getAllDedits();
    console.log(this.allDebits);
   }

  ngOnInit() {
  }
  getAllDedits(){
    this.walletApi.getDebits().subscribe(data => {
      this.allDebits = data.result;
    })
  }

}
