
import { Component,OnInit, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';


import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';


import {  Betslip } from '../../models/betslips';
import { ActivityService } from '../../services/activity.service';




// user: User;
// predictions: string;
// up_kicks: number;
// down_kicks: number;
// title: string;
// sales: number;
// active: boolean;
// date_created: string;
// free: boolean;
// }

const Betslips: Betslip[] = [];

function search(text: string, pipe: PipeTransform): Betslip[] {
  return Betslips.filter(country => {
    const term = text.toLowerCase();
    return country.title.toLowerCase().includes(term)
        || pipe.transform(country.sales).includes(term)
        || pipe.transform(country.id).includes(term);
  });
}

@Component({
  selector: 'app-mybetslips',
  templateUrl: './mybetslips.component.html',
  styleUrls: ['./mybetslips.component.scss'],
  providers: [ DecimalPipe, ]
})
export class MybetslipsComponent implements OnInit {

  betslips$: Observable<Betslip[]>;
  // filter = new FormControl('');
  constructor(pipe: DecimalPipe, private activityApi: ActivityService) { 
   this.getMyBetslips();
   
  }

  getMyBetslips(){
    return this.activityApi.getbetslips().subscribe(data => {
      
      this.betslips$ = data.results
      console.log(this.betslips$);
    })
  }

  ngOnInit() {
  }

}
