import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MybetslipsComponent } from './mybetslips.component';

describe('MybetslipsComponent', () => {
  let component: MybetslipsComponent;
  let fixture: ComponentFixture<MybetslipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MybetslipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MybetslipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
