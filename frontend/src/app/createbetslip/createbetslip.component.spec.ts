import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatebetslipComponent } from './createbetslip.component';

describe('CreatebetslipComponent', () => {
  let component: CreatebetslipComponent;
  let fixture: ComponentFixture<CreatebetslipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatebetslipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatebetslipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
