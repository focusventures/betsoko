import { Component, OnInit } from '@angular/core';
import { GamesService } from '../../services/games.service';
import { Observable } from 'rxjs';




@Component({
  selector: 'app-createbetslip',
  templateUrl: './createbetslip.component.html',
  styleUrls: ['./createbetslip.component.scss'],
  
})

export class CreatebetslipComponent implements OnInit {


  fixtures = [{"one": "o1111"}];

  constructor(private gamesApi: GamesService) {
    this.getFixtures();
    

   }

  ngOnInit() {
  }

  getFixtures = () => {
    this.gamesApi.get_today_fixtures()
    .subscribe(
      data => {
        console.log(data);
        this.fixtures = data.results
      },
      error => {
        console.log(error)
      }
    )
  }

}
