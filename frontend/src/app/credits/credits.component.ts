import { Component, OnInit } from '@angular/core';
import { WalletService } from '../../services/wallet.service';

@Component({
  selector: 'app-credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.scss']
})
export class CreditsComponent implements OnInit {
  allCredits = [];
  constructor(private walletApi: WalletService) { 
    this.getAllCredits();
    console.log(this.allCredits);
  }

  ngOnInit() {
  }

  getAllCredits(){
    this.walletApi.getCredits().subscribe(data => {
      this.allCredits = data.result;
    })
  }


}
