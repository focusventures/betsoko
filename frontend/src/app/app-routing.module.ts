import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { PasswordconfirmComponent } from './passwordconfirm/passwordconfirm.component';
import { CreatebetslipComponent } from './createbetslip/createbetslip.component';
import { ProfileComponent } from './profile/profile.component';
import { WalletComponent } from './wallet/wallet.component';
import { DebitsComponent } from './debits/debits.component';
import { CreditsComponent } from './credits/credits.component';
import { BetslipdetailsComponent } from './betslipdetails/betslipdetails.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { MybetslipsComponent } from './mybetslips/mybetslips.component';
import { BetslipsComponent } from './betslips/betslips.component';
import { FreebetslipsComponent } from './freebetslips/freebetslips.component';
import { PremiumbetslipsComponent } from './premiumbetslips/premiumbetslips.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'betslips/create', component: CreatebetslipComponent },
  { path: 'about', component: AboutComponent },
  { path: 'profile', component: ProfileComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'signin', component: LoginComponent},
  { path: 'passwordreset', component: PasswordresetComponent},
  { path: 'passwordconfirm', component: PasswordconfirmComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'users/:username', component: UserdetailsComponent},
  { path: 'wallet', component: WalletComponent },
  { path: 'wallet/debits', component: DebitsComponent },
  { path: 'wallet/credits', component: CreditsComponent },
  { path: 'betslips/detail/:id', component: BetslipdetailsComponent },
  // { path: 'betslips', component: BetslipsComponent },
  { path: 'betslips/free', component: FreebetslipsComponent },
  { path: 'betslips/premium', component: PremiumbetslipsComponent },
  { path: 'betslips/mybetslips', component: MybetslipsComponent },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
