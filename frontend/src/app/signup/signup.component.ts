import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  submitted = false;
  success = false;

  constructor(private  formBuilder: FormBuilder) { 
    this.signupForm = this.formBuilder.group({
      username: ['', Validators.required],
      phonenumber: ['', Validators.required],
      password: ["", Validators.required],
      confirm_password: ["", Validators.required],
    })
  }

  onSubmit(){
    this.submitted = true;

    if(this.signupForm.invalid){
      return;
    }
    console.log(this.signupForm);

    this.success = true;
  }

  ngOnInit() {
  }

}
