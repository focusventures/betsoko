import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../../services/activity.service';
import { WalletService } from '../../services/wallet.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  betslips = [];
  bets = [1, 2, 3, 4, 5, 6, 7]
  note = "";
  constructor(private activityApi: ActivityService, private walletAPi: WalletService) { 
    this.betslip();
  }

  ngOnInit() {
  }

  betslip(){
    this.activityApi.getbetslips().subscribe(data =>{
      console.log(data);
      this.betslips = data.results;
    }, 
    error => {
      console.log(error);
    })
  }

  betslipUpkick(id){
    console.log("kicked Up", id);
    
    this.activityApi.upkick({"id": id}).subscribe(data => {
      console.log(data);
    })

  }

  betslipDownkick(){

  }

  betslipBuy(id){

    this.walletAPi.buySlip(id).subscribe(res => {
      
      console.log(res);
      this.note = res['detail'];
    })
    
    
  }
  betslipdetail(id){

    this.walletAPi.buySlip(id).subscribe(res => {
      
      console.log(res);
      this.note = res['detail'];
    })
    
    
  }

  

}
