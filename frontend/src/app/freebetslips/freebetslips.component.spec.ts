import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreebetslipsComponent } from './freebetslips.component';

describe('FreebetslipsComponent', () => {
  let component: FreebetslipsComponent;
  let fixture: ComponentFixture<FreebetslipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreebetslipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreebetslipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
