import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { PasswordresetComponent } from './passwordreset/passwordreset.component';
import { PasswordconfirmComponent } from './passwordconfirm/passwordconfirm.component';
import { HomeComponent } from './home/home.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { FooterComponent } from './shared/footer/footer.component';
import { CreatebetslipComponent } from './createbetslip/createbetslip.component';
import { ProfileComponent } from './profile/profile.component';
import { MybetslipsComponent } from './mybetslips/mybetslips.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import { WalletComponent } from './wallet/wallet.component';
import { DebitsComponent } from './debits/debits.component';
import { CreditsComponent } from './credits/credits.component';
import { BetslipdetailsComponent } from './betslipdetails/betslipdetails.component';
import { BetslipsComponent } from './betslips/betslips.component';
import { FreebetslipsComponent } from './freebetslips/freebetslips.component';
import { PremiumbetslipsComponent } from './premiumbetslips/premiumbetslips.component';
import { MainspaceComponent } from './mainspace/mainspace.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ContactComponent,
    LoginComponent,
    SignupComponent,
    PasswordresetComponent,
    PasswordconfirmComponent,
    HomeComponent,
    SidemenuComponent,
    NavbarComponent,
    CreatebetslipComponent,
    ProfileComponent,
    MybetslipsComponent,
    UserdetailsComponent,
    WalletComponent,
    DebitsComponent,
    CreditsComponent,
    BetslipdetailsComponent,
    BetslipsComponent,
    FreebetslipsComponent,
    PremiumbetslipsComponent,
    FooterComponent,
    MainspaceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
