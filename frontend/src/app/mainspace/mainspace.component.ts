import { Component, OnInit } from '@angular/core';
import { GamesService } from '../../services/games.service';

@Component({
  selector: 'app-mainspace',
  templateUrl: './mainspace.component.html',
  styleUrls: ['./mainspace.component.scss']
})
export class MainspaceComponent implements OnInit {
  betslipCreated = false;
  betslipSaved = true;


  fixtures = [];
  constructor(private gamesApi: GamesService) {

    this.getFixtures();
   }

  ngOnInit() {
  }

  getFixtures(){
    this.gamesApi.get_today_fixtures().subscribe(data => {
      console.log(data);
      this.fixtures = data.results;
    })
  }
  createBetslip(name){
    this.betslipCreated = true;
    this.betslipSaved = false;

  }
  
  addToBetslip(id){

  }
  removeFromBetslip(id){

  }

  deleteBetslip(id){

  }

}
