import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetslipsComponent } from './betslips.component';

describe('BetslipsComponent', () => {
  let component: BetslipsComponent;
  let fixture: ComponentFixture<BetslipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetslipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetslipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
