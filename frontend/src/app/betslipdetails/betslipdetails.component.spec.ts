import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetslipdetailsComponent } from './betslipdetails.component';

describe('BetslipdetailsComponent', () => {
  let component: BetslipdetailsComponent;
  let fixture: ComponentFixture<BetslipdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetslipdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetslipdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
