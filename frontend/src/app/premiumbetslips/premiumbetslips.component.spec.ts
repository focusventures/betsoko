import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumbetslipsComponent } from './premiumbetslips.component';

describe('PremiumbetslipsComponent', () => {
  let component: PremiumbetslipsComponent;
  let fixture: ComponentFixture<PremiumbetslipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumbetslipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumbetslipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
