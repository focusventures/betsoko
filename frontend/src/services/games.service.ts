import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Fixture } from '../models/fixtures'
import { base_url } from '../app/globals';


@Injectable({
  providedIn: 'root'
})
export class GamesService {
  httpHeaders = new HttpHeaders({'Content-Type':'application/json'})

  constructor(private http: HttpClient) { 
  }
  get_today_fixtures(): Observable<any>{
    return this.http.get(base_url + '/fixtures/', {headers: this.httpHeaders});


  };
}
