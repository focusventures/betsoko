import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User, Profile } from '../models/users';
import { base_url } from '../app/globals';
import { httpHeaders } from '../app/globals';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private authService: HttpClient) { }

  login(): Observable<any> {
    return this.authService.post(base_url + '/api-auth/', {headers : httpHeaders})
  }

  register(): Observable<any> {
    return this.authService.post(base_url + '/register/', {headers : httpHeaders})
  }

}
