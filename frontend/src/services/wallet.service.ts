import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { base_url, httpHeaders } from '../app/globals';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WalletService {

  constructor(private http: HttpClient) { }


  getDebits(): Observable<any>{
    return this.http.get(base_url + '/wallet/debits/', { headers: httpHeaders} )

  }

  getCredits(): Observable<any>{
    return this.http.get(base_url + '/wallet/credits/', { headers: httpHeaders} )

  }

  getBalance(): Observable<any>{
    return this.http.get(base_url + '/wallet/balance/', { headers: httpHeaders} )

  }

  buySlip(id): Observable<any>{
    var body = {"id": id}
    console.log(body);
    return this.http.post(base_url + '/wallet/buy',JSON.stringify(body), { headers: httpHeaders} )

  }

}
