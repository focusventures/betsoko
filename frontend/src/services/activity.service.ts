import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { base_url, httpHeaders } from '../app/globals';
import { Betslip } from '../models/betslips';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {

  httpHeaders = new HttpHeaders({'Content-Type':'application/json'})

  constructor(private http: HttpClient) { 
  }
  getbetslips(): Observable<any>{
    return this.http.get(base_url + '/betslips/', {headers: this.httpHeaders});

  };

  createbetslip(): Observable<any>{
    return this.http.post(base_url + '/create', {headers: httpHeaders})
  };

  addPredictionToSlip() {
    
  }

  upkick(data): Observable<any>{
    console.log(data);
    return this.http.post(base_url + '/betslips/upkicks', data)
  }


  downkick(id){

  }

  getMybetslips(): Observable<any>{
    return this.http.get(base_url + '/betslips/mybetslips', {headers: this.httpHeaders});

  };

 

  
}
