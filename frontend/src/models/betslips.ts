import { User } from './users';


export class Betslip {
    user: User;
    id: number;
    predictions: string;
    up_kicks: number;
    down_kicks: number;
    title: string;
    sales: number;
    active: boolean;
    date_created: string;
    free: boolean;
}