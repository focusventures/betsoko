


export class Fixture {
    fixture_id: number;
    event_timestamp: string;
    event_date:string;
    league_id: number;
    fixture_round: string;
    homeTeam : string;
    awayTeam :string;
    status : string;
    statusShort: string;
    goalsHomeTeam:number;
    goalsAwayTeam:number;
    halftime_score: number;
    final_score: number;
    penalty:number;
    elapsed: string;
    firstHalfstart:string;
    

}