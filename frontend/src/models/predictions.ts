import { Fixture } from './fixtures'; 
import { User } from './users';

export class Prediction {
    fixture: Fixture;
    created_by: User;
    created_on: string;
    status: string;
    result: string;
}