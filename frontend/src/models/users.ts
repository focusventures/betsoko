

export class User {
    username: string;
    firstname: string;
    lastname: string;
    phonenumber: string;
}

export class Profile {
    user: User;
    profile_pic: string;
}