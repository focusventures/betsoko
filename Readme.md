Requirements
-python 3.6.7
-Django==2.1.7
-django-cors-headers==2.5.2
-djangorestframework==3.9.2


Installation
-Install python 3.6.7
-create virtualenv with virtualenv

e.g virtualenv env

-Activate the env

e.g env\scripts\activate
-

-Clone the repo
-move in to the cloned repo

-install requirements

e.g pip install -r requirements.txt

-Get data

e.g python manage.py data --season year

-Run the server
